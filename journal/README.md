# Personal Journal

## General Roadmap
1. Creating a login form to connect your SSH server (and a logout button)
2. Writing a ssh_client class to handle SSH connection
3. Add http session cookie to maintain and authenticate the web connection
4. Writing monitoring functions to return information about specific details
5. Displaying the monitoring information in different web pages 

-------------
## Technologies
* [Docker](https://docs.docker.com/engine/install/ubuntu/) -
the project can be ran inside a docker container
* [Flask](https://flask.palletsprojects.com/en/2.0.x/) - web app was built using flask

------------
## Problems i ran into
### SSH connection in python 
I tried to figure a way to connect to SSH through python, and decided to use the subproccess module. After some unsuccessful attempts to keep my ssh session I decided to move to a prebuilt module called Paramiko. This allowed my work with ssh to be easier.

### Viewing command history
Generally, in debian based systems all bash history is stored in ~/.bash_history  (zsh at ~/.zsh_history). The problem with it is that everyone can view and change those logs. The solution is to change the file writing permissions. But still, it isn't the perfect solution because if a user gets root permission we are screwed. 

Im still working on a better way to achieve this command logging using bash trap or using audit.d .

### Dynamic HTML page
To display my monitoring information I had to add some kind of parameters to my html script. To do that turns out flask has a default web template engine Jinja that llows customization of tags, filters, tests, and globals. Learning this I could add conditions, call functions and print objects from my html page.

### Access host machine from a docker container
When running my container, I tried to connect to my localhost ssh service. In order to make that happen I had to look for the gateway of this container.
To solve this I configured my own network at docker-compose.yml, and there I set the gateway address. So now if I wanted to access port 22 on my localhost from the container, I could access the default gateway at port 22.

-----------

## What have i learned
It has been a very fun and interesting project, I learned ton of new things, especially in web development and the logging system in linux:

* Building my own web application in python - I learned a lot of new concepts in Flask Framework like working with blueprints (removed it from here), flask login, sessions, Jinja. I have awakend my HTML and CSS skills from back when I was a little boy, it was truely exciting!

* Linux logging - I have got to learn more about the logging system in linux by working with files at the /var/log/ directory. I'll know for the future where to look for connections to my machine, different sudo commands, and more.

* Docker network - I learned about the different kinds of networks of a docker container and configured my own bridge network.
-------------
## Conclusion
I had the creative freedom to take this project to any direction (as long as it stays in the security spectrum), and I am glad I chose this direction. I liked the proccess of creating a website, the data analysis I did, the new things I learned along the way.

Although I still have much more to improve, add, and change, I think I did a good job in the limited time I had. It has been a rough month, but I still managed to dedicate time to this project and come up with a product.

----------
## Special Thanks
Special tanks to Rezilion for the warm hosting, to Yasha for beeing the person he is and for leading the group, and to all the great lecturers. It has been super fun!
