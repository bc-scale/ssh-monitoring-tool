# SSH Monitoring Tool



CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Launch
 * References
 
------------------------
INTRODUCTION
------------------------
>SSH is a powerful tool that provides a secure channel over an unsecured network. 
Yet it can become dangerous and cause serious damage when it is not properly managed. Attackers can try grabbing SSH keys or use privilege escalation to gain control and eventually get root permissions.

My solution is to make a monitoring tool for the sysadmin, where he can inspect ssh connections, watch logs, be alerted on malicious activity (like kill syscalls or unprivileged user sudo) and more.
This tool is supposed to help detect malicious connections and take-care/report about them.

### Please note that the current version doesn't support all features mentioned above.

-----------------
REQUIREMENTS
------------------

This module requires the following modules:

* Python 3.6 +
* [Flask](https://flask.palletsprojects.com/en/2.0.x/)
 * [Docker](https://docs.docker.com/engine/install/ubuntu/)
 * [Docker Compose](https://docs.docker.com/compose/)
 * [OpenSSH Server](https://ubuntu.com/server/docs/service-openssh)
 
--------------
INSTALLATION
------------
Installation of the OpenSSH client and server applications is simple. To install the OpenSSH server:

```bash
sudo apt install openssh-server
```

Install required python modules. Docker will install it automatically when being built.

```bash
pip3 install -r requirements.txt
```

--------------
LAUNCH
-------------
Make sure you have a ssh service to connect to, and connect to a user with enough permissions.
Start your own sshd daemon by running (change configurations at ~/.ssh/config):

```bash
sudo systemctl restart sshd.service
``` 

> note that this project does not include SSH Keys. Login with password.

-------

You can run the project with docker-compose.yml by executing:

```bash
docker-compose up --build
``` 

### OR

You can run the app directly using python
```bash
python3 ./app.py
``` 

------

### Enter the web application at http://localhost:5000/

#### When connecting from docker container be aware that the gateway address 192.168.45.1 if you want to access SSH on your host machine.


--------
REFERENCES
---------------
- [SSH Essentials: Working with SSH Servers, Clients, and Keys](https://www.digitalocean.com/community/tutorials/ssh-essentials-working-with-ssh-servers-clients-and-keys)
- [view command history of another user in Linux](https://superuser.com/questions/309434/how-to-view-command-history-of-another-user-in-linux/309435)
- [How To Add Authentication to Your App with Flask-Login](https://www.digitalocean.com/community/tutorials/how-to-add-authentication-to-your-app-with-flask-login)